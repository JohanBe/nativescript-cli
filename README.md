# nativescript-cli

This repo includes nativescript running on ubuntu-18.04 LTS.

It includes android sdk and latest node.js v10.x.

It allows you with a single command to build your entire nativescript application for android.

Perfect for CI environments like jenkins or bitbucket pipelines.

# reproducability

For reproducable builds I added version branches / tags on docker hub.

Branch version/18.04-10.x-6.4.0-28.0.3-1.20.1

Is creating a Docker container with

- Ubuntu 18.04 LTS
- Node.js v10.x
- Nativescript CLI 6.4.0
- Android SDK 28.0.3
- Nativescript Cloud 1.20.1

If you need more version combinations just create PRs on Bitbucket

# running android with version/18.04-10.x-6.4.0-28.0.3-1.20.1
`docker run -v $(pwd):/app -it scratchy/nativescript-cli:18.04-10.x-6.4.0-28.0.3-1.20.1 bash -C "cd /app && tns build android"`
